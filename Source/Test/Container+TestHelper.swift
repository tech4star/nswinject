import Foundation
import Spry
import Swinject

import NSwinject

public extension Container {
    @discardableResult
    func register<Service>(_ serviceType: Service.Type,
                           instance: Service,
                           name: String? = nil) -> ServiceEntry<Service> {
        return register(serviceType,
                        name: name,
                        factory: { _ in instance })
    }
}
