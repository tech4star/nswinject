import Foundation
import UIKit
import Swinject
import SwinjectStoryboard

import Quick
import Nimble
import Spry
import Spry_Nimble

@testable import NSwinject
@testable import NSwinjectTestHelpers

class NSwinjectAssemblySpec: QuickSpec {
    override func spec() {
        describe("NSwinjectAssembly") {
            var subject: Resolver!
            var viewControllerFactory: NViewControllerFactory!

            beforeEach {
                let container = Container()
                container.storyboardInitCompleted(TestViewController.self) { _, _ in }

                subject = Assembler([NSwinjectAssembly()], container: container).resolver
                viewControllerFactory = subject.forceResolve()
            }

            it("should not be nil") {
                expect(subject).toNot(beNil())
                expect(viewControllerFactory).toNot(beNil())
            }

            describe("UINavigationController") {
                var viewController: (UINavigationController, TestViewController)?

                beforeEach {
                    viewController = viewControllerFactory.createNavigationController()
                }

                it("should not be nil") {
                    expect(viewController?.0.viewControllers.isEmpty).to(beFalse())
                    expect(viewController?.1).toNot(beNil())
                }
            }

            describe("NViewControllerFactory") {
                var resolvedDependancy: NViewControllerFactory?

                beforeEach {
                    resolvedDependancy = subject.forceResolve()
                }

                it("should not be nil") {
                    expect(resolvedDependancy).toNot(beNil())
                }
            }
        }
    }
}
