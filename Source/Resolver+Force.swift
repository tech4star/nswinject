import Foundation
import Swinject

public extension Resolver {
    func resolve<Service>() -> Service? {
        resolve(Service.self)
    }

    func forceResolve<T>() -> T {
        let obj = resolve(T.self)
        assert(obj != nil, "\(T.self) is not registered")
        return obj.unsafelyUnwrapped
    }

    func forceResolve<T>(_ type: T.Type) -> T {
        let obj = resolve(type)
        assert(obj != nil, "\(type) is not registered")
        return obj.unsafelyUnwrapped
    }

    func forceResolve<T>(name: String) -> T {
        let obj = resolve(T.self, name: name)
        assert(obj != nil, "\(T.self) \(name) is not registered")
        return obj.unsafelyUnwrapped
    }

    func forceResolve<T>(_ type: T.Type, name: String) -> T {
        let obj = resolve(type, name: name)
        assert(obj != nil, "\(type) \(name) is not registered")
        return obj.unsafelyUnwrapped
    }

    func forceResolve<T, Arg1>(_ type: T.Type, argument: Arg1) -> T {
        let obj = resolve(type, argument: argument)
        assert(obj != nil, "\(type) \(argument) is not registered")
        return obj.unsafelyUnwrapped
    }

    func forceResolve<T, Arg1>(argument: Arg1) -> T {
        let obj = resolve(T.self, argument: argument)
        assert(obj != nil, "\(T.self) \(argument) is not registered")
        return obj.unsafelyUnwrapped
    }
}
