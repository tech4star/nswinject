import Foundation
import UIKit

import Swinject
import SwinjectStoryboard

public class NSwinjectAssembly: Assembly {
    public init() {
    }

    public func assemble(container: Container) {
        container.register(NViewControllerFactory.self) { [weak container] _ in
            guard let container = container else {
                assert(false, "Oops, we hit a snag")
                return NViewControllerFactoryImpl(container: Container())
            }

            return NViewControllerFactoryImpl(container: container)
        }

        container.storyboardInitCompleted(UINavigationController.self) { _, _ in }
    }
}
